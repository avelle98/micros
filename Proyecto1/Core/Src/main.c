/* USER CODE BEGIN Header */
/**
  **************************
  * @file           : main.c
  * @brief          : Main program body
  **************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  **************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
volatile int button1_int = 0;
volatile int button2_int = 0;
uint32_t local_time, sensor_time;
uint32_t distance;

uint32_t hcsr04_read (void) { //lectura sensor ultrasonidos
	local_time=0;
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);  // el trigger emite los ultrasonidos
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

	while (!(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6)));  // el echo recibe el sonido rebotado
	while (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6)) {
		local_time++;   // se calcula el tiempo que tarda en llegar rebotado el ultrasonido
		}
	return local_time; // la función devuelve el tiempo
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
 	/* Prevent unused argument(s) compilation warning */
 	UNUSED(htim);

 	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_11); //Parpadeo de la alarma
 }

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == GPIO_PIN_5) //botón control led interior
		button1_int = 1;
	if (GPIO_Pin == GPIO_PIN_2) //botón habilitación de la alarma
		button2_int = 1;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t adcValue; //valor leído por el conversor A/D del LDR
  //variables de los antirrebotes
  uint8_t button1_count = 0;
  uint8_t button2_count = 0;
  int counter1 = HAL_GetTick();
  int counter2 = HAL_GetTick();
  int counter_ir; //temporizador led control por IR
  int lumD13 = 0; //estado de luminosidad del led interior
  int alarm = 0; //estado activación de la alarma
  int enable_alarm = 0; //estado habilitación de la alarma

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
  HAL_NVIC_DisableIRQ(TIM2_IRQn);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, 0);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  //LECTURA CONVERSOR A/D DEL LDR
	  HAL_ADC_Start(&hadc1);

	  if (HAL_ADC_PollForConversion(&hadc1, 10) == HAL_OK)
		  adcValue = HAL_ADC_GetValue(&hadc1); //valor leído

	  HAL_ADC_Stop(&hadc1);

	  //CONTROL LED DE LA FAROLA
	  if(adcValue > 0xDC) //si se supera el valor umbral
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 1); //led de la farola activado
	  else
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0); //led de la farola desactivado

	  //CONTROL LED INTERIOR
	  if (button1_int == 1) { //atención a la interrupción
		  HAL_NVIC_DisableIRQ(EXTI0_IRQn); //se deshabilitan las interrupciones
		  //antirrebotes
		  if (HAL_GetTick() - counter1 >= 20) {
			  counter1 = HAL_GetTick();
			  if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5) != 1)
				  button1_count = 0;
			  else
				  button1_count++;
			  if (button1_count == 3) {
				  button1_count = 0;
				  //INSTRUCCIONES
				  switch (lumD13) {
				  case 0: //led apagado
					  __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, 90); //ciclo de trabajo al 90%
					  lumD13++; //se avanza de estado
					  break;
				  case 1:
					  __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, 10); //ciclo de trabajo al 10%
					  lumD13++; //se avanza de estado
					  break;
				  case 2:
					  __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, 0); //ciclo de trabajo al 0%
					  lumD13 = 0; //se avanza de estado
					  break;
				  }
				  button1_int = 0;
			  }
		  }
		  HAL_NVIC_EnableIRQ(EXTI0_IRQn); //se vuelven a habilitar las interrupciones
	  }

	  //CONTROL ALARMA
	  //(1) Control por pulsador
	  if (button2_int == 1) { //atención a la interrupción
		  HAL_NVIC_DisableIRQ(EXTI0_IRQn); //se deshabilitan las interrupciones
		  //antirrebotes
		  if (HAL_GetTick() - counter2 >= 20) {
			  counter2 = HAL_GetTick();
			  if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2) != 1)
				  button2_count = 0;
			  else
				  button2_count++;
			  if (button2_count == 3) {
				  button2_count = 0;
				  //INSTRUCCIONES
				  if(alarm == 0 && enable_alarm == 1) { //si alarma desactivada y habilitada
					  enable_alarm = 0; //alarma deshabilitada
					  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0); //led indicador de la habiltación de la alarma apagado
				  }
				  else {
					  if(alarm == 0 && enable_alarm == 0) { //si alarma desactivada y deshabilitada
						  enable_alarm = 1; //alarma habilitada
						  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 1); //led indicador de la habiltación de la alarma encendido
					  }
				  }
				  if(alarm == 1 && enable_alarm == 1) { //si alarma activada y habilitada
					  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, 0); //salidas alarma desactivadas
					  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0); //led indicador de la habiltación de la alarma apagado
					  HAL_NVIC_DisableIRQ(TIM2_IRQn); //se deshabilita el temporizador de la alarma
					  alarm = 0; //alarma desactivada
					  enable_alarm = 0; //alarma deshabilitada
				  }
				  button2_int = 0;
			  }
		  }
		  HAL_NVIC_EnableIRQ(EXTI0_IRQn); //se vuelven a habilitar las interrupciones
	  }
	  //(2) Control por sensor de ultrasonidos
	  sensor_time = hcsr04_read();
	  distance  = sensor_time * .034/2; // convertimos el tiempo tardado en rebotar el sonido en distancia al objeto

	  if (distance>0 && distance<10 && alarm == 0
		  && enable_alarm == 1) //distancia entre 0 y 10cm & alarma desactivada & alarma habilitada
	  {
		  HAL_NVIC_EnableIRQ(TIM2_IRQn); //se habilitan las interrupciones del temporizador conectado a la alarma
		  alarm = 1; //alarma activada
	  }

	  //CONTROL LED DEL PORTAL (infrarrojos)
	  if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_3)) { //si no se detecta presencia en el sensor de IR
		  if (HAL_GetTick() - counter_ir >= 3000) //si han pasado 3s
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 0); //led apagado
	  }
	  else { //si se detecta presencia
		  counter_ir = HAL_GetTick(); //se resetea el temporizador
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 1); //led encendido
	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 16000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 500;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 16;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 100;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA0 PA2 PA5 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PD11 PD12 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PC6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
